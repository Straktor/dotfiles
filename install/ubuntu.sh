#!/bin/bash

# sudo apt-get install git-core
# git clone https://gitlab.com/Straktor/dotfiles.git ~/.dotfiles

# Install ZSH
sudo apt-get install zsh -y
wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
chsh -s /bin/zsh

# Install tmux
sudo apt-get install tmux -y 

# Install vim
sudo apt-get install vim-gtk -y
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/vundle

# Create symlinks
ln -s ~/.dotfiles/zsh/zshrc ~/.zshrc
ln -s ~/.dotfiles/tmux/tmux.conf ~/.tmux.conf
ln -s ~/.dotfiles/vim/vimrc ~/.vimrc